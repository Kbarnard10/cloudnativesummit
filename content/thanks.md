---
title: Thanks
---

### You're the best 

We'd like this to use this space to thank all the folks contributing to the Virtual Cloud Native Summit even if they are not on the “stage” online. 

1. [Melissa Smolensky](https://twitter.com/melsmo) for sparking the idea
1. [Priyanka Sharma](https://twitter.com/pritianka) for leading the charge to put this event together
1. [Kaitlyn Barnard](https://twitter.com/kaitlyn_barnard) for jumping in to help with the event on day one
1. [Abubakar Siddiq Ango](https://twitter.com/sarki247) for building this website
2. [Ariel Jatib](https://twitter.com/arieljatib) for helping organize, designing and building the website
3. [Kim McMahon](https://twitter.com/kamcmahon) for bringing in the CNCF as an organizer
4. [Puja Abbassi](https://twitter.com/puja108) for being the liaison between the event and the K8s Contributor Summit
5. [Chris Short](https://twitter.com/ChrisShort) for getting us started and requisitioning the Zoom account from CNCF
6. [Chris Kühl](https://twitter.com/rejektsio) and [Matt Baldwin](https://twitter.com/baldwinmathew) for their support
7. [Zaki Mahomed](https://twitter.com/zakimahomed) for helping us envision a virtual event experience that engages and informs at the same time; also, for manning the show's graphic design needs  
8. [James Strong](https://twitter.com/strongjz) for creating and running the unique event production using gaming software OBS. We could not have done this without you!

