# Cloud Native Summit Online Website
The website is built with [Nanoc](https://nanoc.ws), hosted using [GitLab Pages]() and currently accessible using the domain name https://cloudnativesummit.online

## Website Directory Structure

The most important directories to be aware of are:

- `content` - All the content that is rendered on the final output are here
- `data` - JSON files storing event data like speakers and supporters
- `layouts` - template files used by all other pages
- `lib` - Ruby code used to generate certain content like redering JSON Data

## Making Changes

### Home Page (HTML/ERB)

File: `content/index.html`

Most content on the page are HTML, but it contains a few ERB content to render Featured Speakers and Supporters.

### Schedule (HTML)

File: `content/schedule.html`

It currently contains embed data for sched.com event management page

### Speakers (HTML/ERB)

File: `content/speakers.html`

Just like Index page it has an ERB to render all speakers from `data/talks.json` file.

### Support Us, Thanks, Code of COnduct Pages (Markdown)

Files: `content/*.md`

To ease editing since its mainly text, markdown is used for these pages.


### Adding Supporters

In the `data/event.json` file, inside in the `supporters` JSON array, you can add new supporters using the following format:

```json
    {
        "name": "GitLab", 
        "logo_url": "https://events.linuxfoundation.org/wp-content/uploads/gitlab-spn.svg",
        "website": "https://about.gitlab.com/", 
        "organizer": true
     }
```

Setting the `organizer` attribute to `true` will make the supported listed on the pages as an organizer, while `false` lists them as a supporter

### Adding Speakers

In `data/talks.json`, you can add a new speaker inside the `speakers` JSON array using the format:

```json
        {
            "slug": "richard",
            "name": "Richard ‘RichiH’ Hartmann",
            "bio": "",
            "affiliation": "Prometheus core team member and Grafana Labs Director",
            "image_url": "https://grafana.com/static/img/about/richard_hartmann.jpg",
            "featured": true,
            "social_media": 
                {
                    "twitter": "", 
                    "website": "", 
                    "medium": "", 
                    "linkedin": "", 
                    "others": ""
                }
        },
```

Setting `featured` to `true` lists the speaker on the homepage as a featured speaker while `false` lists them as spakers on the speakers page.

## How Sign Up is handled

[Mailchimp PopUp](https://mailchimp.com/resources/fresh-new-pop-up-forms-to-grow-your-list/) is currently used to capture registration data, and its tweaked on the website to allow the pop up launch everytime in the case of a single person registering for multiple people, by setting set the cookie Mailchimp uses to check if the user has previously subscribed to expire in `layouts/default.html`:

```
document.cookie = "MCPopupSubscribed=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
```
